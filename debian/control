Source: nemo
Section: misc
Priority: optional
Maintainer: Debian Cinnamon Team <pkg-cinnamon-team@lists.alioth.debian.org>
Uploaders:
 Maximiliano Curia <maxy@debian.org>,
 Margarita Manterola <marga@debian.org>,
 Fabio Fantoni <fantonifabio@tiscali.it>,
Build-Depends:
 autoconf-archive,
 automake,
 cinnamon-l10n (>= 3.0),
 debhelper (>= 9),
 dh-autoreconf,
 dh-python,
 gobject-introspection,
 gtk-doc-tools (>= 1.4),
 intltool (>= 0.40.1),
 libatk1.0-dev (>= 1.32.0),
 libcinnamon-desktop-dev (>= 2.6.1),
 libexempi-dev (>= 2.1.0),
 libexif-dev (>= 0.6.20),
 libgail-3-dev,
 libgirepository1.0-dev (>= 0.9.12),
 libglib2.0-dev (>= 2.37.3),
 libglib2.0-doc,
 libgtk-3-dev (>= 3.9.10),
 libgtk-3-doc,
 libnotify-dev (>= 0.7.0),
 libx11-dev,
 libxext-dev,
 libxml2-dev (>= 2.7.8),
 libxrender-dev,
 python,
 python3,
 python3-gi,
 python3-polib,
 x11proto-core-dev,
Standards-Version: 3.9.8
Homepage: http://cinnamon.linuxmint.com/
Vcs-Browser: https://anonscm.debian.org/git/pkg-cinnamon/nemo.git
Vcs-Git: https://anonscm.debian.org/git/pkg-cinnamon/nemo.git

Package: nemo
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends:
 desktop-file-utils (>= 0.7),
 gsettings-desktop-schemas,
 gvfs (>= 1.3.2),
 libcinnamon-desktop4 (>= 2.6.1),
 libglib2.0-data,
 libnemo-extension1 (= ${binary:Version}),
 nemo-data (= ${source:Version}),
 shared-mime-info (>= 0.50),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 cinnamon-l10n,
 gvfs-backends,
 gvfs-fuse,
 librsvg2-common,
 nemo-fileroller,
Suggests: eog, evince | pdf-viewer, totem | mp3-decoder, xdg-user-dirs
Description: File manager and graphical shell for Cinnamon
 Nemo is the official file manager for the Cinnamon desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the Cinnamon
 desktop. It works on local and remote filesystems.
 .
 Several icon themes and components for viewing different kinds of files
 are available in separate packages.

Package: libnemo-extension1
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Nemo component system facilities
 Nemo is the official file manager for the Cinnamon desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the Cinnamon
 desktop. It works on local and remote filesystems.
 .
 This package contains the shared library needed by the nemo extensions.

Package: libnemo-extension-dev
Section: libdevel
Architecture: any
Depends:
 gir1.2-nemo-3.0 (= ${binary:Version}),
 libglib2.0-dev (>= 2.37.3),
 libgtk-3-dev (>= 3.9.10),
 libnemo-extension1 (= ${binary:Version}),
 ${misc:Depends},
Description: Nemo component system facilities (development files)
 Nemo is the official file manager for the Cinnamon desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the Cinnamon
 desktop. It works on local and remote filesystems.
 .
 This package contains the files needed to compile and link nemo extensions.

Package: gir1.2-nemo-3.0
Section: introspection
Architecture: any
Depends: ${gir:Depends}, ${misc:Depends}
Description: libraries for nemo components - gir bindings
 Nemo is the official file manager for the Cinnamon desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the Cinnamon
 desktop. It works on local and remote filesystems.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings.

Package: nemo-data
Architecture: all
Depends: python, ${misc:Depends}, ${python:Depends}
Suggests: nemo
Description: File manager and graphical shell for Cinnamon (data files)
 Nemo is the official file manager for the Cinnamon desktop. It allows one
 to browse directories, preview files and launch applications associated
 with them. It is also responsible for handling the icons on the Cinnamon
 desktop. It works on local and remote filesystems.
 .
 This package contains pictures, localization files and other data
 needed by nemo.
